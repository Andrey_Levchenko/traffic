package base;

import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Group;

public class Main extends Application {
    public static void main(String[] args) {
        Application.launch(args);
    }

    private Group mainGroup;

    @Override
    public void start(Stage stage) {
        stage.setTitle("Traffic");
        stage.setWidth(1600);
        stage.setHeight(900);

        mainGroup = new Group();
        Scene scene = new Scene(mainGroup);
        stage.setScene(scene);

        run();
        stage.show();
    }

    private void run() {
        RoadGrid grid = new RoadGrid(mainGroup);
        grid.addNode(new RoadNode(450.d, 450.d));
        grid.addNode(new RoadNode(410.d, 110.d));
        grid.addNode(new RoadNode(760.d, 420.d));
        grid.addNode(new RoadNode(420.d, 780.d));
        grid.addNode(new RoadNode(150.d, 450.d));
        grid.addNode(new RoadNode(780.d, 710.d));

        grid.addRoad(0, 1, 3);
        grid.addRoad(0, 2, 2);
        grid.addRoad(0, 3, 2);
        grid.addRoad(0, 4, 1);
        grid.addRoad(2, 5, 2);
        grid.addRoad(3, 5, 2);

        grid.drawRoads();
    }
}