package base;

import java.awt.geom.Point2D;

public class Lane {

    private Point2D.Double[] vertices;

    Lane(Point2D.Double begin, Point2D.Double end) {
        vertices = new Point2D.Double[2];
        vertices[0] = begin;
        vertices[1] = end;
    }

    Point2D.Double[] getVertices() {
        return vertices;
    }
}
