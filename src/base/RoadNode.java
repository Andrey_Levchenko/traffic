package base;

import java.awt.geom.Point2D;

public class RoadNode {
    private Point2D.Double position;

    public RoadNode(Point2D.Double position) {
        this.position = position;
    }

    public RoadNode(double x, double y) {
        position = new Point2D.Double(x, y);
    }

    public void setPosition(Point2D.Double position) {
        this.position = position;
    }

    public Point2D.Double getPosition() {
        return position;
    }

    public boolean same(RoadNode node) {
        return position.getX() == node.position.getX() && position.getY() == node.position.getY();
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null || getClass() != obj.getClass())
            return false;
        RoadNode node = (RoadNode) obj;
        return position.equals(node.position);
    }

    @Override
    public int hashCode() {
        return position.hashCode();
    }
}