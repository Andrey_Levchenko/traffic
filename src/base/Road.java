package base;

import geometry.Angle;
import geometry.GeometricLine;
import javafx.scene.Group;
import javafx.scene.shape.Line;
import java.awt.geom.Point2D;

class Road {
    private final static double LANEWIDTH = 25;

    private RoadNode begin;
    private RoadNode end;
    private int laneCount;
    private Group group;

    private GeometricLine borderLeft;
    private GeometricLine borderRight;
    private GeometricLine borderEnd;
    private GeometricLine borderBegin;
    private GeometricLine[] trafficLine;
    private boolean borderIsSet;

    Road(RoadNode begin, RoadNode end, int laneCount, Group group) {
        this.begin = begin;
        this.end = end;
        this.laneCount = laneCount;
        this.group = group;

        trafficLine = new GeometricLine[laneCount * 2 - 1];
        borderIsSet = false;
    }

    void setBorderLeftRight(boolean isBegin) {
        double deltaX = end.getPosition().x - begin.getPosition().x;
        double deltaY = end.getPosition().y - begin.getPosition().y;
        if (!isBegin) {
            deltaX = -deltaX;
            deltaY = -deltaY;
        }

        double angle = Angle.angle(
                new Point2D.Double(1.d, 0.d),
                new Point2D.Double(0.d, 0.d),
                new Point2D.Double(deltaX, deltaY)
        );

        double dx = Math.cos(angle + Math.PI / 2.d) * LANEWIDTH * laneCount;
        double dy = Math.sin(angle + Math.PI / 2.d) * LANEWIDTH * laneCount;

        if (!isBegin) {
            dx = -dx;
            dy = -dy;
        }

        borderRight = new GeometricLine(
                new Point2D.Double(begin.getPosition().x + dx, begin.getPosition().y + dy),
                new Point2D.Double(end.getPosition().x + dx, end.getPosition().y + dy));

        borderLeft = new GeometricLine(
                new Point2D.Double(begin.getPosition().x - dx, begin.getPosition().y - dy),
                new Point2D.Double(end.getPosition().x - dx, end.getPosition().y - dy));

        borderIsSet = true;
}

    void setBordersBeginEnd() {
        if (borderIsSet) {
            borderEnd = new GeometricLine(borderLeft.getEnd(), borderRight.getEnd());
            borderBegin = new GeometricLine(borderLeft.getBegin(), borderRight.getBegin());
        }
        else {
            borderEnd = new GeometricLine(new Point2D.Double(), new Point2D.Double());
            borderBegin = new GeometricLine(new Point2D.Double(), new Point2D.Double());
        }
    }

    void setTrafficLine() {
        double deltaX = end.getPosition().x - begin.getPosition().x;
        double deltaY = end.getPosition().y - begin.getPosition().y;

        double angle = Angle.angle(
                new Point2D.Double(1.d, 0.d),
                new Point2D.Double(0.d, 0.d),
                new Point2D.Double(deltaX, deltaY)
        );

        for (int i = 0; i < trafficLine.length; ++i) {
            int k = i - trafficLine.length / 2;

            double dx = Math.cos(angle + Math.PI / 2.d) * LANEWIDTH * k;
            double dy = Math.sin(angle + Math.PI / 2.d) * LANEWIDTH * k;

            trafficLine[i] = new GeometricLine(
                    new Point2D.Double(begin.getPosition().x + dx, begin.getPosition().y + dy),
                    new Point2D.Double(end.getPosition().x + dx, end.getPosition().y + dy));

            trafficLine[i].clipThis(borderBegin, true);
            trafficLine[i].clipThis(borderEnd, false);
        }
    }

    void drawRoad() {
        drawLine(borderLeft, false);
        drawLine(borderRight, false);

        for (int i = 0; i < trafficLine.length; ++i) {
            if (i != trafficLine.length / 2) {
                drawLine(trafficLine[i], true);
            }
            else {
                drawLine(trafficLine[i], false);
            }
        }
    }

    private void drawLine(GeometricLine geometricLine, boolean dash) {
        Line line = new Line(
                geometricLine.getBegin().x,
                geometricLine.getBegin().y,
                geometricLine.getEnd().x,
                geometricLine.getEnd().y
        );

        if (dash) {
            line.getStrokeDashArray().addAll(25.d, 15.d);
        }

        group.getChildren().add(line);
    }

    RoadNode getBegin() {
        return begin;
    }

    RoadNode getEnd() {
        return end;
    }

    GeometricLine getBorderLeft() {
        return borderLeft;
    }

    GeometricLine getBorderRight() {
        return borderRight;
    }

    GeometricLine getBorderEnd() {
        return borderEnd;
    }

    GeometricLine getBorderBegin() {
        return borderBegin;
    }

    boolean getBorderIsSet()  {
        return borderIsSet;
    }
}