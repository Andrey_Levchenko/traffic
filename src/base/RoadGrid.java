package base;

import geometry.Angle;
import geometry.GeometricLine;
import javafx.scene.Group;

import java.awt.geom.Point2D;
import java.util.ArrayList;

class RoadGrid {
    private ArrayList<RoadNode> nodes;
    private ArrayList<Road> roads;
    private Group group;

    RoadGrid(Group group) {
        this.group = group;
        nodes = new ArrayList<>();
        roads = new ArrayList<>();
    }

    void addNode(RoadNode node) {
        boolean same = false;
        for (RoadNode it : nodes) {
            if (it.same(node)) {
                same = true;
            }
        }
        if (same)
            return;
        nodes.add(node);
    }

    void addRoad(int begin, int end, int laneCount) {
        if (begin < nodes.size() && end < nodes.size() && begin != end) {
            Road road = new Road(nodes.get(begin), nodes.get(end), laneCount, group);
            roads.add(road);
        }
    }

    void drawRoads() {
        //Проектируем перекрестки
        for (RoadNode node : nodes) {
            designCrossing(node);
        }

        //Определяем грани начала и конца дороги и рисуем дороги
        for (Road road : roads) {
            road.setBordersBeginEnd();
            road.setTrafficLine();
            road.drawRoad();
        }
    }

    private void designCrossing(RoadNode node) {
        ArrayList<Road> incoming = new ArrayList<>();
        ArrayList<Boolean> isBegin = new ArrayList<>();

        //Ищем дороги входящие в узел, а также определяем начало ли это
        for (Road road : roads) {
            if (node.equals(road.getBegin()) || node.equals(road.getEnd())) {
                incoming.add(road);

                if (node.equals(road.getBegin())) {
                    isBegin.add(true);
                    if (!road.getBorderIsSet()) {
                        road.setBorderLeftRight(true);
                    }
                }
                else {
                    isBegin.add(false);
                    if (!road.getBorderIsSet()) {
                        road.setBorderLeftRight(false);
                    }
                }
            }
        }

        //Проектируем углы
        if (incoming.size() > 1) {
            designCorner(incoming, isBegin);
        }
    }

    private void designCorner(ArrayList<Road> roads, ArrayList<Boolean> isBegin) {
        int[] left = new int[roads.size()];

        //Находим первую дорогу слева для каждой из дорог
        for (int i = 0; i < roads.size(); ++i) {
            int indexMin = roads.size();
            double angleMin = 2 * Math.PI;

            for (int j = 0; j < roads.size(); ++j) {
                if (i != j) {
                    Point2D.Double p1;
                    Point2D.Double p2;
                    Point2D.Double p3;

                    if (isBegin.get(i)) {
                        p1 = roads.get(i).getEnd().getPosition();
                        p2 = roads.get(i).getBegin().getPosition();
                    } else {
                        p1 = roads.get(i).getBegin().getPosition();
                        p2 = roads.get(i).getEnd().getPosition();
                    }

                    if (isBegin.get(j)) {
                        p3 = roads.get(j).getEnd().getPosition();
                    } else {
                        p3 = roads.get(j).getBegin().getPosition();
                    }

                    double angle = Angle.angle(p1, p2, p3);
                    if (angle < angleMin) {
                        angleMin = angle;
                        indexMin = j;
                    }
                }
            }
            left[i] = indexMin;
        }

        //Определяем пересекаемые границы дорог
        for (int i = 0; i < roads.size(); ++i) {
            GeometricLine line1;
            GeometricLine line2;

            if (isBegin.get(i)) {
                line1 = roads.get(i).getBorderRight();
            }
            else {
                line1 = roads.get(i).getBorderLeft();
            }

            if (isBegin.get(left[i])) {
                line2 = roads.get(left[i]).getBorderLeft();
            }
            else {
                line2 = roads.get(left[i]).getBorderRight();
            }

            line1.clip(line2, isBegin.get(i), isBegin.get(left[i]));
        }
    }
}
