package geometry;

import java.awt.geom.Point2D;

public class GeometricLine implements Cloneable {
    private Point2D.Double begin;
    private Point2D.Double end;

    public GeometricLine(Point2D.Double begin, Point2D.Double end) {
        this.begin = begin;
        this.end = end;
    }

    public GeometricLine clone() throws CloneNotSupportedException {
        return (GeometricLine) super.clone();
    }
    
    public void setBegin(Point2D.Double begin) {
        this.begin = begin;
    }

    public void setEnd(Point2D.Double a) {
        this.end = a;
    }

    public void increase(double coefficient, boolean toEnd) {
        if (toEnd) {
            Point2D.Double vector = new Point2D.Double(end.x - begin.x, end.y - begin.y);
            end.x = begin.x + vector.x * coefficient;
            end.y = begin.y + vector.y * coefficient;
        }
        else {
            Point2D.Double vector = new Point2D.Double(begin.x - end.x, begin.y - end.y);
            begin.x = end.x + vector.x * coefficient;
            begin.y = end.y + vector.y * coefficient;
        }
    }

    public void clip(GeometricLine line, boolean isBegin1, boolean isBegin2) {
        Point2D.Double p = this.intersection(line);
        if (isBegin1) {
            begin = p;
        }
        else {
            end = p;
        }

        if (isBegin2) {
            line.begin = p;
        }
        else {
            line.end = p;
        }
    }

    public void clipThis(GeometricLine line, boolean isBegin) {
        Point2D.Double p = this.intersection(line);
        if (isBegin) {
            begin = p;
        }
        else {
            end = p;
        }
    }

    public Point2D.Double getBegin() {
        return begin;
    }

    public Point2D.Double getEnd() {
        return end;
    }

    public Point2D.Double intersection(GeometricLine line) {
        Point2D.Double result = new Point2D.Double(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);

        double a1 = this.begin.getY() - this.end.getY();
        double b1 = this.end.getX() - this.begin.getX();
        double c1 = this.begin.getX() * this.end.getY() - this.end.getX() * this.begin.getY();
        double a2 = line.begin.getY() - line.end.getY();
        double b2 = line.end.getX() - line.begin.getX();
        double c2 = line.begin.getX() * line.end.getY() - line.end.getX() * line.begin.getY();

        double delta = a1 * b2 - a2 * b1;
        if (delta == 0.d) {
            return result;
        }
        else {
            double deltaX = c2 * b1 - c1 * b2;
            double deltaY = a2 * c1 - a1 * c2;
            result.setLocation(deltaX / delta, deltaY / delta);
            return result;
        }
    }
}