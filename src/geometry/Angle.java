package geometry;

import java.awt.geom.Point2D;

public class Angle {
    public static double angle(Point2D.Double p1, Point2D.Double p2, Point2D.Double p3) {
        Point2D.Double a = new Point2D.Double(p1.x - p2.x, p1.y - p2.y);
        Point2D.Double b = new Point2D.Double(p3.x - p2.x, p3.y - p2.y);

        double aa = a.distance(0.d, 0.d);
        double bb = b.distance(0.d, 0.d);
        double cos = (a.x * b.x + a.y * b.y) / (aa * bb);
        double sin = (a.x * b.y - a.y * b.x) / (aa * bb);

        if (Math.abs(cos) > 1.d) {
            cos -= cos % 1.d;
        }
        if (Math.abs(sin) > 1.d) {
            sin -= sin % 1.d;
        }

        if (sin > 0.d) {
            return Math.acos(cos);
        }
        else {
            return 2 * Math.PI - Math.acos(cos);
        }
    }
}
