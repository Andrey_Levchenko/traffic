package tests;

import geometry.GeometricLine;
import org.junit.Test;

import java.awt.geom.Point2D;
import java.math.BigDecimal;
import java.math.RoundingMode;

import static org.junit.Assert.*;

public class GeometricLineTest {
    @Test
    public void cloneTest() throws CloneNotSupportedException {
        GeometricLine line1 = new GeometricLine(new Point2D.Double(10.d, 20.d), new Point2D.Double(30.d, 40.d));
        GeometricLine line2 = line1.clone();
        assertEquals(line1.getBegin(), line2.getBegin());
        assertEquals(line1.getEnd(), line2.getEnd());
    }

    @Test
    public void SetBegin() {
        GeometricLine line = new GeometricLine(new Point2D.Double(0.d, 0.d), new Point2D.Double(0.d, 0.d));
        line.setBegin(new Point2D.Double(10.d, 20.d));
        assertEquals(new Point2D.Double(10.d, 20.d), line.getBegin());
    }

    @Test
    public void SetEnd() {
        GeometricLine line = new GeometricLine(new Point2D.Double(0.d, 0.d), new Point2D.Double(0.d, 0.d));
        line.setEnd(new Point2D.Double(10.d, 20.d));
        assertEquals(new Point2D.Double(10.d, 20.d), line.getEnd());
    }

    @Test
    public void increase() {
        GeometricLine line1 = new GeometricLine(new Point2D.Double(10.d, 20.d), new Point2D.Double(30.d, 40.d));
        GeometricLine line2 = new GeometricLine(new Point2D.Double(10.d, 20.d), new Point2D.Double(50.d, 60.d));
        line1.increase(2.d, true);
        assertEquals(line1.getEnd(), line2.getEnd());
    }

    @Test
    public void clip() throws CloneNotSupportedException {
        GeometricLine a = new GeometricLine(new Point2D.Double(10.d, 20.d), new Point2D.Double(30.d, 40.d));
        GeometricLine b = new GeometricLine(new Point2D.Double(50.d, 30.d), new Point2D.Double(10.d, 70.d));

        GeometricLine line1 = a.clone();
        GeometricLine line2 = b.clone();
        line1.clip(line2, true, true);
        assertEquals(line1.intersection(line2), line1.getBegin());
        assertEquals(line1.intersection(line2), line2.getBegin());

        line1 = a.clone();
        line2 = b.clone();
        line1.clip(line2, true, false);
        assertEquals(line1.intersection(line2), line1.getBegin());
        assertEquals(line1.intersection(line2), line2.getEnd());

        line1 = a.clone();
        line2 = b.clone();
        line1.clip(line2, false, true);
        assertEquals(line1.intersection(line2), line1.getEnd());
        assertEquals(line1.intersection(line2), line2.getBegin());

        line1 = a.clone();
        line2 = b.clone();
        line1.clip(line2, false, false);
        assertEquals(line1.intersection(line2), line1.getEnd());
        assertEquals(line1.intersection(line2), line2.getEnd());
    }

    @Test
    public void clipThis() throws CloneNotSupportedException {
        GeometricLine a = new GeometricLine(new Point2D.Double(10.d, 20.d), new Point2D.Double(30.d, 40.d));
        GeometricLine b = new GeometricLine(new Point2D.Double(50.d, 30.d), new Point2D.Double(10.d, 70.d));

        GeometricLine line1 = a.clone();
        GeometricLine line2 = b.clone();
        line1.clipThis(line2, true);
        assertEquals(line1.intersection(line2), line1.getBegin());

        line1 = a.clone();
        line2 = b.clone();
        line1.clipThis(line2, false);
        assertEquals(line1.intersection(line2), line1.getEnd());
    }

    @Test
    public void intersection() {
        geometry.GeometricLine a = new geometry.GeometricLine(new Point2D.Double(3.d, 1.d), new Point2D.Double(2.d, 5.d));
        geometry.GeometricLine b = new geometry.GeometricLine(new Point2D.Double(0.d, 7.d), new Point2D.Double(-2.d, -4.d));
        Point2D.Double p1 = a.intersection(b);
        p1.setLocation(
                new BigDecimal(p1.getX()).setScale(10, RoundingMode.DOWN).doubleValue(),
                new BigDecimal(p1.getY()).setScale(10, RoundingMode.DOWN).doubleValue());
        Point2D.Double p2 = new Point2D.Double(0.6315789473d,10.4736842105d);

        assertEquals(p2, p1);

        geometry.GeometricLine c = new geometry.GeometricLine(new Point2D.Double(3.d, 5.d), new Point2D.Double(2.d, 5.d));
        geometry.GeometricLine d = new geometry.GeometricLine(new Point2D.Double(0.d, 7.d), new Point2D.Double(-2.d, 7.d));
        Point2D.Double p3 = c.intersection(d);
        Point2D.Double p4 = new Point2D.Double(Double.POSITIVE_INFINITY, Double.POSITIVE_INFINITY);
        assertEquals(p3, p4);
    }
}