package tests;

import base.RoadNode;
import org.junit.Test;

import java.awt.geom.Point2D;

import static org.junit.Assert.*;

public class RoadNodeTest {

    @Test
    public void setPosition() {
        RoadNode node = new RoadNode(20.d, 5.d);
        assertEquals(node.getPosition(), new Point2D.Double(20.d, 5.d));
    }

    @Test
    public void same() {
        RoadNode node1 = new RoadNode(20.d, 5.d);
        RoadNode node2 = new RoadNode(20.d, 5.d);
        RoadNode node3 = new RoadNode(10.d, 70.d);

        assertTrue(node1.same(node2));
        assertFalse(node1.same(node3));
    }

    @Test
    public void testEquals() {
        RoadNode node1 = new RoadNode(20.d, 5.d);
        RoadNode node2 = new RoadNode(20.d, 5.d);
        RoadNode node3 = new RoadNode(10.d, 70.d);

        assertEquals(node1, node2);
        assertNotEquals(node1, node3);
    }

    @Test
    public void testHashCode() {
        RoadNode node1 = new RoadNode(20.d, 5.d);
        RoadNode node2 = new RoadNode(20.d, 5.d);
        RoadNode node3 = new RoadNode(10.d, 70.d);

        assertEquals(node1.hashCode(), node2.hashCode());
        assertNotEquals(node1.hashCode(), node3.hashCode());
        assertNotEquals(node2.hashCode(), node3.hashCode());
    }
}