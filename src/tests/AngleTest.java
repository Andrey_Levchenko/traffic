package tests;

import geometry.Angle;
import org.junit.Test;

import java.awt.geom.Point2D;

import static org.junit.Assert.*;

public class AngleTest {

    @Test
    public void angle() {
        Point2D.Double p1 = new Point2D.Double(1, 0);
        Point2D.Double p2 = new Point2D.Double(0, 0);
        Point2D.Double p3 = new Point2D.Double(0, 1);
        Point2D.Double p4 = new Point2D.Double(1, 1);

        assertEquals(Math.PI / 2, Angle.angle(p1, p2, p3), 0.00001d);
        assertEquals(2 * Math.PI, Angle.angle(p1, p2, p1), 0.00001d);
        assertEquals(Math.PI / 4, Angle.angle(p1, p2, p4), 0.00001d);
    }
}