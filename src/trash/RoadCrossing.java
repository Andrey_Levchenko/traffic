package trash;

import base.RoadNode;
import geometry.GeometricLine;

import java.awt.geom.Point2D;

public class RoadCrossing {
    private RoadNode crossing;
    private Point2D.Double[] vertices;
    private GeometricLine[] border;

    RoadCrossing(RoadNode crossing) {
        this.crossing = crossing;
    }

    RoadNode getCrossing() {
        return crossing;
    }

    Point2D.Double[] getVertices() {
        return vertices;
    }

    GeometricLine[] getBorder() {
        return border;
    }
}
